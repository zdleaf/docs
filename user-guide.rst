**********
User Guide
**********

.. section-numbering::
    :suffix: .

.. contents::

Notice
======

The Morello software stack consists of capability-aware firmware components,
kernel and Android built with native Android build system.
This environment also provides a BusyBox based minimal filesystem using the
same firmware components and kernel to demonstrate the platform features.
It also provides a Debian 11 based filesystem that can be used to validate the
pure-cap Linux kernel environment and test the pure-cap applications.

.. _prerequisites:

Host prerequisites for a validated build environment
====================================================

- Ubuntu Linux 20.04 LTS running on an x86_64 machine.
- Minimum 16GB of RAM and 250GB free storage space.
- Commands provided in this guide are executed from a ``bash`` shell
  environment.


Packages
--------

To ensure that all the required packages are installed, run:

::

    sudo apt-get update
    sudo apt-get install autoconf autopoint bc binfmt-support bison \
        build-essential ca-certificates cpio curl debootstrap \
        device-tree-compiler docker.io dosfstools doxygen fdisk flex gdisk \
        gettext-base git libncurses5 libssl-dev libtinfo5 \
        linux-libc-dev-arm64-cross lsb-release m4 mtools pkg-config \
        python-is-python3 python3-distutils qemu-user-static rsync snapd \
        unzip uuid-dev wget

To ensure that all the required packages for FVP are installed, run:

::

    sudo apt-get install telnet xterm

Refer to `Android pre-requisites`_ for more information on packages required to
build Android.

When intended to work on the Linux Pure-cap Environment, also run the following
commands as the first time setup for docker as this environment is built inside
a docker.

::

    sudo groupadd docker
    sudo usermod -aG docker $USER
    sudo chmod 666 /var/run/docker.sock

CMake
-----

While ``cmake`` is available to install with ``apt-get``, this
does not install the required version i.e. 3.18.4 or later.
Following commands can be used for checking the cmake version and removing
it if the version is less than 3.18.4.

::

    sudo cmake --version
    sudo apt-get remove cmake

An alternative way to install the cmake tool is by running the
following command.

::

    sudo snap install cmake --classic

Repo
----

Follow the steps mentioned below to install the ``repo`` tool that are
validated for morello/release-1.5

::

    mkdir ~/bin/
    PATH="${HOME}/bin:${PATH}"
    export REPO=$(mktemp /tmp/repo.XXXXXXXXX)
    curl -o ${REPO} https://storage.googleapis.com/git-repo-downloads/repo
    gpg --keyserver keys.openpgp.org --recv-key 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
    curl -s https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - ${REPO} && install -m 755 ${REPO} ~/bin/repo


**Note:** In case of any failure or for the latest set of instructions to
install the ``repo`` tool, refer to the `AOSP guide`_.

The ``repo`` tool uses ``git`` to download the source code which
should be configured before using the ``repo`` tool.

::

    git config --global user.name "Your Name"
    git config --global user.email "you@example.com"


Syncing the Source Code
=======================

Create a new folder that will be your workspace.
All the instructions below assume that this directory is the workspace
directory.

::

    mkdir <morello_workspace>
    cd <morello_workspace>


Run the following commands to fetch the software stack sources.

**Note:** To have a shallow fetch of the stack use ``--depth=1`` with the
following *repo init* command. This fastens the repo sync process but results
in providing the limited commit history for the projects fetched.

::

    repo init \
        -u https://git.morello-project.org/morello/manifest.git \
        -b <BRANCH> \
        -g <GROUP>
    repo sync

Supported options for ``<BRANCH>`` are:

* morello/release-<major>.<minor>
  (This option fetches the release branch of a given major and minor version.
  The latest release branch is ``morello/release-1.5``)
* refs/tags/<TAG>
  (This option fetches a particular release tag pointed by the <TAG> field.
  For example: refs/tags/morello-release-1.5.rc1)
* morello/mainline
  (This option fetches the mainline branch used for code development and could
  contain patches that are not available in the above branches/tags.)

Supported options for ``<GROUP>`` are:

* android
* bsp
* busybox
* linux-purecap-env

The ``-g <GROUP>`` option is to avoid downloading all components for all
supported software stacks and save time and storage space.
<GROUP> is a comma separated option where multiple options can be provided
as comma separated entries as below:

* -g bsp (Only downloads software components required for BSP)
* -g busybox (Downloads software components for both BSP and BusyBox)
* -g android (Downloads software components for both BSP and Android)
* -g linux-purecap-env (Downloads software components for both BSP and Debian)
* -g busybox,android,linux-purecap-env (Downloads software components for BSP,
  BusyBox, Android and Debian)

**Note:** ``repo sync`` may take significant amount of time to sync the
software stack based on the host machine and network connectivity.

Building the Source Code
========================

All software components are built using a set of bash based build scripts which
are checked out under `<morello_workspace>/build-scripts` directory. These
scripts are common for both FVP and Development Board and the
build is differentiated using build arguments which is explained in
the coming section.

There are separate scripts available to build each software component and
scripts to package the binaries that can be used directly on the
target platform.

Check dependencies
------------------

Before starting the software build, ensure that all the required packages are
installed on the host machine by running the following command from
`<morello_workspace>` directory.

::

    ./build-scripts/check_dep.sh


**Note:** Add the argument ``fvp`` to the above mentioned command for checking
the FVP specific dependencies as well.

A message of "no missing dependencies detected" indicates that all the
required packages are installed on the host machine.

Building the Software Stack
---------------------------

To build all the software components for a given platform and filesystem, a top
level `build-all.sh` script is provided.
Execute the following command to trigger the software build.

::

    ./build-scripts/build-all.sh -p <PLATFORM_TYPE> -f <FILESYSTEM> <CMD>

Supported options for ``<PLATFORM_TYPE>`` are:

* fvp
* soc

If -p flag is not passed, the build scripts default to ``fvp``
for ``PLATFORM_TYPE``.

Supported options for ``<FILESYSTEM>`` are:

* android-nano
* android-swr
* busybox
* debian
* none

Filesystem option ``none`` builds and packages only the firmware components.

Supported options for ``<CMD>`` are:

* clean
* build
* all

If ``CMD`` option is not passed, ``build`` is performed by default.

**Note:** If -p flag is changed, ``clean`` should be performed before a
``build``.

Once the build is complete, the build artifacts are available under
``<morello_workspace>/output`` directory.

Following section provides the build commands for building different filesystem
options supported in this software release.

Firmware only
^^^^^^^^^^^^^

Use this option to build only the firmware components.

**Note:** If user wants to build AArch64 firmware binaries (firmware without
Morello capability features), the build configuration flag
``stack_morello_capabilities`` (which is defined in
``<morello_workspace>/build-scripts/config/bsp`` file) should be set to 0 and
the entire stack should be rebuild. AArch64 firmware binaries are only
compatible with OS built without Morello capability features.

::

    ./build-scripts/build-all.sh -p <fvp/soc> -f none

Android
^^^^^^^

This stack includes support for building different Android profiles.


Android Nano (android-nano)
"""""""""""""""""""""""""""

This profile is a bare-bone console-only environment suitable for testing
terminal-only applications.

::

    ./build-scripts/build-all.sh -p <fvp/soc> -f android-nano

Android SWR (android-swr)
"""""""""""""""""""""""""

This profile provides a full Android configuration booting to Home screen.
The graphics are not accelerated and `SwiftShader`_ is used for software
rendering instead.

::

    ./build-scripts/build-all.sh -p <fvp/soc> -f android-swr

For information on Android boot and further details, please refer to `Android
readme`_.

Ubuntu Distribution
^^^^^^^^^^^^^^^^^^^

Morello platform provides support for installation and boot of standard Ubuntu
20.04 Distribution. The distribution is installed on a SATA disk and since the
installed image is persistent it can be used for multiple boots.

**Note:** This distribution does not contain any capability-aware software. It
is a standard AArch64 image to support an environment for future development.

Build the firmware files:

::

    ./build-scripts/build-all.sh -p <fvp/soc> -f none

For information regarding distribution installation and boot
refer to `distribution readme`_.

Minimal BusyBox
^^^^^^^^^^^^^^^

An AArch64 based BusyBox initramfs to demonstrate ACPI and DT boot, and the
platform features

**Note:** Only the Linux kernel is capability-aware whereas BusyBox binary
is AArch64.

Build BusyBox:

::

    ./build-scripts/build-all.sh -p <fvp/soc> -f busybox

Debian with Linux Pure-cap Environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The AArch64 Debian environment provides pure-cap Morello BusyBox filesystem
along with the Morello sample pure-cap applications. The Morello BusyBox and
pure-cap applications are built using the morello-aarch64 development
environment. For more information on it refer to
`morello aarch64 development environment`_. The morello-aarch64 development
environment provides a pure-cap BusyBox filesystem that embeds the sample
pure-cap applications and is termed as Linux Pure-cap Environment.

Build the Debian distro with a packaged Linux Pure-cap Environment:

::

     ./build-scripts/build-all.sh -p <fvp/soc> -f debian

For more information regarding the Linux Pure-cap Environment on the Morello
platform refer to `morello linux pure-cap environment`_.

Building a standalone baremetal application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The standalone baremetal application can be executed on the Morello platform
as BL33 at EL2 exception level.

Build the firmware files:

::

    ./build-scripts/build-all.sh -p <fvp/soc> -f none

For information regarding Standalone baremetal application build and boot
refer to `Standalone baremetal application readme`_.

Configuring the build options for software components
-----------------------------------------------------

Each software component has different build configurations which are defined in
``<morello_workspace>/build-scripts/config/bsp`` file.
User can modify the required configuration option in the bsp file and can
rebuild the software with the command mentioned before.

The ``<morello_workspace>/build-scripts/config/`` directory also contains the
Grub configuration file for each filesystem using which user can modify the
Linux kernel command line parameters. For Debian, this directory also consists
of the docker file and the required script files needed to embed the Linux
Pure-cap Environment inside the Debian disk image.

Building individual software components
---------------------------------------

The build system supports building individual software components if user has
only modified a given software component to avoid rebuilding the unmodified
components. However the packaging step has to be done manually to generate
the final image containing the updated software component.

The following table shows the command to use to build a given software component
and the corresponding package command to execute to update the build artifacts.

**Note:** For a given filesystem, the whole software stack should be built first
with the build-all.sh script as explained above before building the individual
software components with the commands provided in the table.

.. list-table::
   :widths: 20 150 150
   :header-rows: 1

   * - Software Component
     - Build Command
     - Package Command
   * - SCP
     - ./build-scripts/build-scp.sh -p <fvp/soc> -f <filesystem>
     - ./build-scripts/build-firmware-image.sh -p <fvp/soc> -f <filesystem>
   * - ARM-TF
     - ./build-scripts/build-arm-tf.sh -p <fvp/soc> -f <filesystem>
     - ./build-scripts/build-firmware-image.sh -p <fvp/soc> -f <filesystem>
   * - UEFI
     - ./build-scripts/build-uefi.sh -p <fvp/soc> -f <filesystem>
     - ./build-scripts/build-firmware-image.sh -p <fvp/soc> -f <filesystem>
   * - Linux
     - ./build-scripts/build-linux.sh -p <fvp/soc> -f <filesystem>
     - ./build-scripts/build-disk-image.sh -p <fvp/soc> -f <filesystem>
   * - Android
     - ./build-scripts/build-linux.sh -p <fvp/soc> -f <android-nano/android-swr>
     - ./build-scripts/build-disk-image.sh -p <fvp/soc> -f <android-nano/android-swr>
   * - Busybox
     - ./build-scripts/build-busybox.sh -p <fvp/soc> -f busybox
     - ./build-scripts/build-disk-image.sh -p <fvp/soc> -f busybox
   * - Debian
     - ./build-scripts/build-debian.sh -p <fvp/soc> -f debian
     - ./build-scripts/build-disk-image.sh -p <fvp/soc> -f debian
   * - Device Tree
     - ./build-scripts/build-arm-tf.sh -p <fvp/soc> -f <filesystem>
     - ./build-scripts/build-disk-image.sh -p <fvp/soc> -f <filesystem>
   * - Grub
     - ./build-scripts/build-grub.sh -p <fvp/soc> -f <filesystem>
     - ./build-scripts/build-disk-image.sh -p <fvp/soc> -f <filesystem>

Running the software
====================

Running the software on FVP
---------------------------

The Morello Fixed Virtual Platform (Morello FVP) must be available
to test the software.

Please refer to `Arm Ecosystem FVPs`_ to download the Morello FVP.

**Run scripts:**

``<morello_workspace>/run-scripts/`` provides scripts
for running the software stack on the Morello FVP platform.


The run-scripts structure is as follows:

::

    run-scripts
      |--run_model.sh
      |-- ...

Ensure that all the dependencies are met by executing the
FVP: ``./path/to/FVP_Morello/models/Linux64_GCC-6.4/FVP_Morello``.
You should see the FVP launch, presenting a graphical interface
showing information about the current state of the FVP.

The ``run_model.sh`` script in ``<morello_workspace>/run-scripts``
will launch the FVP, providing the previously built images as arguments.
Execute the ``run_model.sh`` script:

::

        # For running Android Nano:
        ./run-scripts/run_model.sh -m <model binary path> -f android-nano

        # For running Android SWR:
        ./run-scripts/run_model.sh -m <model binary path> -f android-swr

        # For running BusyBox:
        ./run-scripts/run_model.sh -m <model binary path> -f busybox

        # For running Debian:
        ./run-scripts/run_model.sh -m <model binary path> -f debian

When the script is executed, five uart terminal instances will be launched, one
for the SCP, one for MCP and three for the AP (Application Processor). Once the
FVP is running, the SCP will be the first to boot, bringing the AP out of reset.
The AP will start booting Trusted Firmware-A, then UEFI, then Android/BusyBox.

**Note**: Please note that booting Android SWR can take a significant amount of
time. Depending on the hardware on which the FVP is running, it can take
between 45 minutes to 2 hours until the Home screen is displayed.

To run terminal-only mode on hosts without graphics/display:

::

        # For running Android Nano:
        env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f android-nano

        # For running Android SWR:
        env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f android-swr

        # For running BusyBox:
        env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f busybox

        # For running Debian:
        env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f debian

This launches FVP in the background and automatically connects to the
interactive application console via telnet.

To stop the model, exit telnet:

::

        Ctrl + ]
        telnet> close

Running the Linux Pure-cap Environment (Debian)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Run the Debian system with integrated Morello pure-cap applications on the
Morello FVP. Once the boot sequence has been completed, user can login to
it using the following credentials:

**Login ID:** ``root``

**Password:** ``morello``

When the console prompt appears, the user can chroot to the embedded
morello-busybox filesystem and execute each Morello pure-cap
application as described below.

::

        # chroot to morello-busybox
        $ chroot /morello-aarch64/morello/morello-rootfs/ sh

        # To execute morello-helloworld pure-cap application
        /# morello-helloworld

        # To execute morello-stack pure-cap application
        /# morello-stack

        # To execute morello-heap pure-cap application
        /# morello-heap

**Note:** Refer `morello linux pure-cap environment`_ to know more on
Morello's Linux Pure-cap Environment.

Running the Android pure-cap workloads
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To run Android Test Suites and workloads ported to the Pure-cap ABI,
please refer to `Workloads`_.

Running the Android Debug Bridge (ADB)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Run Android on FVP. Once the boot sequence has completed
(when the console prompt appears), use the following commands in another shell
to interact with Android via ADB:

::

        cd <morello_workspace>/android/
        source build/envsetup.sh
        lunch morello_nano-eng   # or morello_swr-eng
        adb devices
        # If no device appears:
        # 1. FVP tries to bind the internal ADB port to port 5555 on the host. This may
        #    fail if the port is in use, in which case the console will show a message like:
        #    > Info: Morello_Top: board.hostbridge: ...binding to port 5559 instead
        #    If this happens, you will need to connect manually using `adb connect 127.0.0.1:<port>`.
        #    You should also use `adb disconnect 127.0.0.1:<port>` before exiting the model.
        #
        # 2. Even when FVP does bind to port 5555 on the host (no port warning on the console),
        #    the ADB host server might fail to detect it. In that case use the instructions
        #    above with <port>=5555.
        adb shell

VirtioP9 Device
^^^^^^^^^^^^^^^

Morello FVP provides support for virtio p9 device. This enables accessing
a directory on the host's filesystem within Linux, or another operating system
that implements the protocol.

In run_model.sh -v, --virtiop9-enable flag is used to enable directory sharing
between host OS and FVP. This flag takes the path to the host directory to be
shared. The directory to be shared must already be present on the host OS.

::

        # Host OS
        mkdir <morello_workspace>/shared_folder
        # If the folder to be shared already exists then no need to create it and
        # just pass the path to it with -v flag.
        ./run-scripts/run_model.sh -m <model binary path> -f <busybox/android-nano/android-swr> -v <path/to/shared_folder>
        #
        # Inside FVP
        mkdir shared_folder
        mount -t 9p -o trans=virtio,version=9p2000.L FM shared_folder

Virtio Net Component
^^^^^^^^^^^^^^^^^^^^

Morello FVP provides support for Virtio Net component along with smc91x
component. Virtio Net provides much better network performance than the
SMSC_91C111 component, because it features host-assisted network acceleration.
This means that it can offload packet processing operations from the simulated
OS on the target, to the host side.

In run_model.sh -e, --ethernet selects the ethernet
driver to use (default: virtio_net).

::

      ./run-scripts/run_model.sh -m <model binary path> -f <busybox/android-nano/android-swr> -e <smc91x/virtio_net>

Setting up TAP interface (optional)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Morello FVP supports ethernet interface to allow networking
support to be usable for the software executed by the FVP. If the
FVP is to be represented as a discrete network entity, the host TAP interface
has to be set up before the FVP is launched. To set up the TAP interface,
execute the following commands on the host machine.

- Install libvirt

  ::

    sudo apt-get install libvirt-daemon-system

- Ensure that the libvirtd service is active.

  ::

    sudo systemctl start libvirtd

- Use the ifconfig command and ensure that a virtual bridge interface named
  'virbrX' (where X is a number 0,1,2,....) is created.

- Create a tap interface named 'tap0'

  ::

    sudo ip tuntap add dev tap0 mode tap user $(whoami)
    sudo ifconfig tap0 0.0.0.0 promisc up
    sudo brctl addif virbr0 tap0

- Launch the model with -t option for TAP interface.

  ::

    ./run-scripts/run_model.sh -m <model binary path> -f <busybox/android-nano/android-swr> -e <smc91x/virtio_net> -t tap0

**Note:** If TAP interface is not set, then user mode networking is enabled by
default. Refer to `User mode networking`_ for more details.

Running the software on Development Board
-----------------------------------------

Please ensure to follow the precautions recommended
in the `Morello Board Precaution Guide`_.

Setting up the Morello Board
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Connect a USB-B cable between the host machine and Morello's
  USB DBG port on the back panel.
* Connect a LAN cable to the PCIe GbE LAN port that is above
  the 2 USB3 host ports.
* Connect an HDMI cable if display is required.
* Connect the supplied C13 power cable to the C14 socket on the rear of
  the platform.
* Switch-on the ATX PSU using the switch next to the C14 socket at the rear of
  the platform.
* After power on, the board will enumerate 8 COM ports (user might need to
  wait for a few minutes, depending on the host system).
* Note down the newly enumerated COM port numbers. Assuming that the COM
  ports start at ttyUSB<n>, following is the COM port assignment:

.. list-table::
   :widths: 25 35 40
   :header-rows: 1

   * - COM Port
     - FTDI device serial number/port
     - Description
   * - ttyUSB<n>
     - 00FT<platform-serial-num>B(0)
     - Motherboard Configuration Controller(MCC)
   * - ttyUSB<n+1>
     - 00FT<platform-serial-num>B(1)
     - Platform Controller Chip(PCC)
   * - ttyUSB<n+2>
     - 00FT<platform-serial-num>B(2)
     - Application Processor(AP)
   * - ttyUSB<n+3>
     - 00FT<platform-serial-num>B(3)
     - System Control Processor(SCP)
   * - ttyUSB<n+4>
     - 00FT<platform-serial-num>A(0)
     - Manageability Control Processor(MCP)
   * - ttyUSB<n+5>
     - 00FT<platform-serial-num>A(1)
     - IOFPGA UART0
   * - ttyUSB<n+6>
     - 00FT<platform-serial-num>A(2)
     - IOFPGA UART1
   * - ttyUSB<n+7>
     - 00FT<platform-serial-num>A(3)
     - AP Secure UART

* Open the MCC, AP and SCP COM ports using a serial port application such as
  *minicom* with the following settings:

      ::

               115200 baud
               8-bit word length
               No parity
               1 stop bit
               No flow control

**Note**: Some serial port applications, including *minicom*, refer to this as
"115200 8N1" or similar.

* An example usage to open the MCC console using *minicom* is as follows:
  (Ensure to have *minicom* package pre-installed to the host using
  ``"sudo apt-get install minicom"``)

  ::

               sudo minicom -s /dev/ttyUSB<n>

  Select ``Serial port setup`` from the user interface and configure the port
  settings mentioned above. Select ``Exit`` to apply the settings and to proceed
  with the port connection.

  Refer to the COM port nomenclature in the table above to choose the desired
  port to establish the connection with. For more information on *minicom*
  usage, refer to `Minicom Manual`_.

* In the MCC console, run USB_ON command. This will launch the microSD card
  in the Morello board as a mass storage device in the host PC.

         ::

               Cmd> USB_ON

* Enter the following command on the MCC console window to ensure date and time
  is correctly set, based on the UTC. This is a one time setting and is required
  when booting the board for the first time. If the date and time values are
  incorrect, set them to the correct UTC-based values.

      ::

             Cmd> debug
             Debug> time
             Debug> date
             Debug> exit

Update firmware on microSD card
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prebuilt board and SoC firmware
"""""""""""""""""""""""""""""""

The board and SoC firmware prebuilt binaries are made available as a part
of the release in the directory ``<morello_workspace>/bsp/board-firmware/``.
These binaries can be copied to the onboard microSD card to boot the
platform to the UEFI menu.

**Note:** Prebuilt binaries of AArch64 firmware (without Morello capability
features) are available under the following directory:
``<morello_workspace>/bsp/board-firmware/SOFTWARE/AARCH64/``.

SoC firmware built from source
""""""""""""""""""""""""""""""

SoC firmware binaries built from source are available under
``<morello_workspace>/output/soc/firmware/``. The binaries from this folder
can be copied to the SOFTWARE folder in microSD card for more recent build
of the firmware or if the firmware source has been modified and rebuilt.

Steps to update microSD card with the firmware binaries
"""""""""""""""""""""""""""""""""""""""""""""""""""""""

* The USB debug cable when connected to the host machine will show
  the microSD partition on the host machine which can be mounted.

      ::

             $> sudo mount /dev/sdx1 /mnt
             $> sudo rm -rf /mnt/*
             $> # For updating prebuilt board and SoC firmware
             $> sudo cp -r <morello_workspace>/bsp/board-firmware/* /mnt/
             $> sudo umount /mnt

**NOTE**: replace ``sdx1`` with the device and partition of the SD card.

* After copying the board firmware, user can see three
  folders (LICENSES, MB and SOFTWARE) and two text files
  (config.txt and ee0364b.txt). The SOFTWARE folder
  contains the last working software binaries that boot till the UEFI menu.
* Copying of board firmware is required only once to get the most up to date
  version of all board images or if the board firmware is updated as part of
  new release tag. The firmware binaries built from source which are available
  under ``<morello_workspace>/output/soc/firmware/`` can be updated to the
  ``/mnt/SOFTWARE/`` directory whenever the firmware components
  (SCP/ARM-TF/UEFI) are modified and rebuilt.
* If user wants to run the prebuilt AArch64 firmware (without Morello
  capability features) then the binaries under
  ``<morello_workspace>/bsp/board-firmware/SOFTWARE/AARCH64/`` can be copied
  to the ``/mnt/SOFTWARE/`` directory. Once copied the board shall be rebooted
  for the new firmware to be booted. AArch64 firmware binaries are only
  compatible with OS built without Morello capability features.
* After copying the binaries run the REBOOT command from the
  MCC console. This will update the board firmware images (if newer),
  power on the supply rails, configure IOFPGA, program SCC registers from
  MB/HBI0364B/io_v010f.txt file and boot the SCP/MCP/AP cores.

    ::

             Cmd> REBOOT

**Note on changing SoC HW/FW configuration:**

- SCP, ARM-TF and UEFI firmware have been designed to be flexible enough in
  configuring the system by parsing the SCC BOOT_GPRx registers. These
  registers are configured by the user in
  ``/mnt/MB/HBI0364B/io_v010f.txt`` file, which will then be programmed by
  the MCC into the SoC SCC registers over serial interface during the MCC
  reboot process.
- More details on how the SCC BOOT_GPR registers are utilized can be found
  within ``/mnt/MB/HBI0364B/io_v010f.txt`` file.


Boot Debian/BusyBox/Android Image
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prepare a bootable disk
"""""""""""""""""""""""

* A bootable disk can be prepared by flashing the image generated from the
  source build onto a USB drive.
* Connect bootable disk (USB drive) into a host machine that
  is running Linux.
* For Debian, the image will be available at the
  location ``<morello_workspace>/output/soc/debian.img``
* For BusyBox, the image will be available at the
  location ``<morello_workspace>/output/soc/busybox.img``
* For Android, the image will be available at the location
  ``<morello_workspace>/output/soc/(android-nano.img/android-swr.img)``
* Use the following commands to prepare the GRUB image on the
  USB drive

        ::

             $ lsblk
             $ sudo dd if=<IMAGE> of=</dev/sdX> conv=fsync bs=1M # write the disk image to
                                                                 # /dev/sdX
             $ sudo sgdisk -e </dev/sdX>                         # adjust the primary and
                                                                 # alternate header on /dev/sdX
             $ sync

**Note:** Replace ``</dev/sdX>`` with the handle corresponding to your USB drive,
as identified by the ``lsblk`` command.

**Note:** All supported images are partitioned using GPT.
For details and issues related to the location of the alternate header, refer
to the `troubleshooting guide`_.

Supported ``<IMAGE>`` names are:

* ``<morello_workspace>/output/soc/android-nano.img``
* ``<morello_workspace>/output/soc/android-swr.img``
* ``<morello_workspace>/output/soc/busybox.img``
* ``<morello_workspace>/output/soc/debian.img``

Booting the board with Debian Image
""""""""""""""""""""""""""""""""""""

Insert the bootable disk created earlier to the Morello board. Reboot the
board by issuing the following command in the MCC console:

    ::

             Cmd> REBOOT

Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc key within
the 10s UEFI boot timeout. Enter the UEFI Boot Manager
menu and select the disk.

By default the Linux kernel will boot with ACPI configuration:

    ::

               | Debian Morello Platform (Device Tree)                                     |
               |*Debian Morello Platform (ACPI)                                            |

The system will boot into a Debian Linux image environment.

Running the Linux Pure-cap Environment (Debian)
***********************************************

Once the boot sequence has been completed, enter the following credentials
to login to the console prompt of the Debian filesystem.

**Login ID:** ``root``

**Password:** ``morello``

Having logged in, use the following commands to chroot into the morello-busyBox
filesystem to test the Morello support-enabled BusyBox and running the pure-cap
applications.

::

        # chroot to morello busybox
        $ chroot /morello-aarch64/morello/morello-rootfs/ sh

        # To execute morello-helloworld pure-cap application
        /# morello-helloworld

        # To execute morello-stack pure-cap application
        /# morello-stack

        # To execute morello-heap pure-cap application
        /# morello-heap

**Note:** Refer `morello linux pure-cap environment`_ to know more on
Morello's Linux Pure-cap Environment.

Booting the board with BusyBox Image
""""""""""""""""""""""""""""""""""""

Insert the bootable disk created earlier to the Morello board. Reboot the
board by issuing the following command in the MCC console:

    ::

             Cmd> REBOOT


Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc key within
the 10s UEFI boot timeout. Enter the UEFI Boot Manager
menu and select the disk.

By default the Linux kernel will boot with ACPI configuration:

    ::

               | BusyBox Morello Platform (Device Tree)                                     |
               |*BusyBox Morello Platform (ACPI)                                            |

The system will boot into a BusyBox Linux image environment.

Booting the board with Android Image
""""""""""""""""""""""""""""""""""""

Insert the bootable disk created earlier to the Morello board. Reboot the
board by issuing the following command in the MCC console:

    ::

             Cmd> REBOOT

Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc
key within the 10s UEFI boot timeout. Enter the UEFI Boot Manager
menu and select the disk.

    ::

               | Booting `Android Morello SoC (Device Tree)'          |

The system will boot into an Android environment.

Running the Android pure-cap workloads
**************************************

To run Android Test Suites and workloads ported to the Pure-cap ABI,
please refer to `Workloads`_.

**Note:** On some occasions, the boot process may hang in the UEFI stage. Refer
to the `troubleshooting guide`_ on how to recover from such a failure.

Network booting
---------------

The Morello UEFI (both SoC and FVP builds) is by default built with support
for `PXE <https://github.com/tianocore/tianocore.github.io/wiki/PXE>`_
(which uses TFTP) and `HTTP
<https://github.com/tianocore/tianocore.github.io/wiki/HTTP-Boot>`_ (but **not**
HTTPS) network booting.
Check the documentation for your distribution of choice on how to configure it
for the network boot.


BusyBox
^^^^^^^
When doing HTTP network boot, the UEFI supports loading a `ramdisk
<https://github.com/tianocore/tianocore.github.io/wiki/HTTP-Boot#ram-disk-boot-from-http>`_
downloaded over the network.
The generated disk image, ``busybox.img``, can be used directly without any modification.

Android
^^^^^^^
Network booting the Morello Android build is not supported.

Using on-board SATA drive
-------------------------

The Morello board has a SATA drive connected to the onboard SATA connector,
secured and packaged in an ATX case.

The SATA drive can be used as a boot device by flashing a bootable disk image
onto it and then booting off it. The prerequisite is that a working Linux kernel
image (preferably busybox) should first be booted into - either by means of a
bootable USB drive or via Network boot - as explained in the earlier sections.

Once booted to Linux, follow the steps below to flash an bootable disk image to
the SATA drive:

    ::

        $ wget -O </dev/sdX> <NETWORK/PATH/TO/IMAGE>      # Fetch and flash
                                                          # the disk image to
                                                          # SATA drive
        $ sync

**Note:** Replace ``</dev/sdX>`` with the handle corresponding to your SATA
drive.

Working with Development Branch
===============================

The development branch ``morello/mainline`` syncs the latest toolchain build
from `Morello LLVM`_ rather than a tagged version.
The first time the build-scripts are executed, the latest toolchain will
be downloaded. *It will however not be automatically updated once downloaded.*

It is recommended after a ``repo sync`` to also check for a newer
`Morello LLVM`_ build.

Run the following command to check and download a new toolchain build
if available:

::

    ./build-scripts/fetch-tools.sh -f none update


In case of any compile or link time issues, it is recommended to check
if a newer build is available.

The toolchain update command mentioned above will check for the availability
of a new toolchain build, and if present, will take a backup of the existing
toolchain by renaming the toolchain directory with ``.old`` suffix. This will
finally be followed by fetching the new toolchain build.
Backing up the existing toolchain provides user the flexibility to switch back
to it in case of any compile or runtime issues with the new toolchain.

.. _Trusted Firmware-A:
 https://trustedfirmware-a.readthedocs.io/en/latest/

.. _SCP Firmware:
 https://github.com/ARM-software/SCP-firmware

.. _edk2:
 https://github.com/tianocore/edk2

.. _edk2-platforms:
 https://github.com/tianocore/edk2-platforms

.. _busybox:
 https://busybox.net/

.. _grub:
 https://www.gnu.org/software/grub/

.. _Android readme:
 android-readme.rst

.. _Workloads:
 workloads.rst

.. _Arm Ecosystem FVPs:
 https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps

.. _distribution readme:
 distro.rst

.. _AOSP guide:
 https://source.android.com/setup/develop#installing-repo

.. _SwiftShader:
 https://opensource.google/projects/swiftshader

.. _Morello LLVM:
 https://git.morello-project.org/morello/llvm-project

.. _Standalone baremetal application readme:
 standalone-baremetal-readme.rst

.. _morello aarch64 development environment:
 https://git.morello-project.org/morello/morello-aarch64

.. _Morello Board Precaution Guide:
 morello-board-precautions.rst

.. _troubleshooting guide:
 troubleshooting-guide.rst

.. _User mode networking:
 https://developer.arm.com/documentation/100964/1116/Introduction-to-the-Fast-Models-Reference-Manual/User-mode-networking

.. _Android pre-requisites:
 https://source.android.com/setup/build/initializing#setting-up-a-linux-build-environment

.. _Minicom Manual:
 http://manpages.ubuntu.com/manpages/trusty/man1/minicom.1.html

.. _morello linux pure-cap environment:
 morello-linux-purecap-environment.rst
