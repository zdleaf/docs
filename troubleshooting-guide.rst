*********************
Troubleshooting Guide
*********************

Introduction
============

The `user guide`_ documentation typically suffices in most cases
but there could be certain host development machine dependencies
that could cause failures. This page provides solutions for known
issues that could affect the use of the platform software stack.

Error while using "repo" command
================================

The ``repo init`` or ``repo sync`` command fails with the below listed
error message.

::

    File "<path-to-workspace>/.repo/repo/main.py", line 79
    file=sys.stderr)
    ^
    SyntaxError: invalid syntax


The typical reason for this failure could be that the default version of python
on the development machine is not python3.6+. To resolve this issue, install the
latest version of python, if not already installed on the development machine
and invoke the repo command from ``~/bin/repo`` with ``python3`` as listed
below.

::

    python3 ~/bin/repo init \
        -u https://git.morello-project.org/morello/manifest.git \
        -b <BRANCH> \
        -g <GROUP>

    python3 ~/bin/repo sync

Alternate GPT header not at the end of the disk
===============================================

``build-disk-image.sh`` creates GPT partitioned disk images, in these images the
alternate GPT header is put at the end of the disk image as per the
GPT specification.

However, when writing these with a raw copy disk write utility (such as ``dd``)
the alternative header will be written according to its location in the image,
not at the end of the disk.
This violates the GPT specification and prevents easy recovery of the partition
table using the alternative header if the primary header has been corrupted.

As long as the primary header is not corrupted the incorrect position of the
alternative header is not an issue.

Example where Linux kernel warns about this situation:
::

    GPT:Primary header thinks Alt. header is not at the end of the disk.
    GPT:1663008 != 30031871
    GPT:Alternate GPT header not at the end of the disk.
    GPT:1663008 != 30031871
    GPT: Use GNU Parted to correct GPT errors.

Note that this is not a fatal error but rather a warning.
The kernel only considers the primary header unless explicitly requested
otherwise with the `gpt parameter`_.

Most partition tools with support for GPT can be used to relocate
the alternative header.
Note that this operation must be performed on the target block device rather
than the image file as the block device size must be known.

Example on how to relocate the alternative header after writing the disk image
using sgdisk from the `gdisk package`_:
::

    dd if=<IMAGE> of=/dev/sdX conv=fsync bs=1M   # write the disk image to /dev/sdX
    sgdisk -e /dev/sdX                           # adjust the primary and alternate
                                                 # header on /dev/sdX

Boot hang in UEFI with ASSERT from VariableRuntimeDxe
=====================================================

This issue, albeit occasional, stems from corruption in the UEFI persistent
storage region. Please execute the following commands on the MCC console to
erase the UEFI persistent storage region.

::

    Cmd> debug
    Debug> selftest
    Debug> R
    Debug> 0
    Debug> exit
    Cmd> REBOOT

This will reset the UEFI persistent storage to the default settings by erasing
the AP QSPI flash and reflashing the FIP region on the next boot, which should
go through successfully.

.. _gpt parameter:
  https://git.morello-project.org/morello/kernel/morello-ack/-/blob/morello/android12-5.10/Documentation/admin-guide/kernel-parameters.txt#L1483

.. _gdisk package:
  https://packages.ubuntu.com/search?keywords=gdisk&searchon=names&exact=1

.. _user guide:
 user-guide.rst

.. _AOSP guide:
 https://source.android.com/setup/develop#installing-repo
