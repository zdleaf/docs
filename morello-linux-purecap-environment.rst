**********************************
Morello Linux Pure-cap Environment
**********************************

**DISCLAIMER:** This is the initial phase of the Linux Pure-cap Environment
enablement and is subject to change in future releases.

Introduction
============

Linux Pure-cap Environment on the Morello platform provides an environment to
build and test the pure-cap BusyBox, known as Morello BusyBox, and pure-cap
applications. The Debian root file-system provide the Linux Pure-cap Environment
which collectively is a morello-aarch64 development environment with prebuilt
Morello BusyBox and Morello pure-cap applications. Users need to boot Debian as
the primary filesystem on Morello SoC/FVP and do chroot to prebuilt (and embedded
within) Morello BusyBox to test the C64 ports and the pure-cap applications. Users
can again build morello-aarch64 sources inside Debian on the Morello SoC, which
generates the new Morello BusyBox filesystem with newly regenerated pure-cap
applications.

Building the Linux Pure-cap Environment
=======================================

The Linux Pure-cap Environment here is the Morello BusyBox filesystem wherein
the Morello sample pure-cap applications are packaged. This environment is
embedded within a Debian RootFS and can be moved into by doing a chroot to the
packaged Morello BusyBox from the booted Debian. A Debian disk image is created
out of this Debian RootFS with the Linux Pure-cap Environment and can be flashed
and booted to the Morello platform just like the other BusyBox and Android images.

The Linux Pure-cap Environment is built using the scripts provided by the
``morello-aarch64`` project in an x86_64 host system. These scripts are executed
as a part of building the Debian image and include the provisions to

* fetch the prebuilt toolchain
* fetch musl-libc source
* build musl with the `libshim` disabled
* build the pure-cap applications using the toolchain and musl binaries
* fetch Morello BusyBox and Morello Linux Kernel Headers
* build Morello BusyBox filesystem
* package the pure-cap applications into the Morello BusyBox filesystem

The following scripts' structure of the morello-aarch64 project handles
the build of the Linux Pure-cap Environment.

    ::

        scripts/
         ├── build-all.sh: main script for fetching building all packages, musl-libc, pure-cap apps, and morello-busybox
         ├── build-busybox.sh: script for fetching and building morello-busybox
         ├── build-musl.sh: script for fetching and building musl-libc without libshim
         ├── build-rootfs.sh: script for packaging the pure-cap applications to morello-busybox filesystem
         └── fetch-toolchain.sh: script for fetching the required toolchain

The Debian RootFS is built using a docker container environment. The docker
is run after the build of Linux Pure-cap Environment and it bootstraps for a
minimal Debian RootFS wherein the prior built Linux Pure-cap Environment is
packaged. The Debian root image created as a part of the docker run is then
made a Debian disk image using the stack provided ``build-disk-image.sh``. The
following steps provision the building of the final Debian disk image with all
the above-mentioned things embedded within in one go.

    ::

        $ cd <morello_workspace>
        $ ./build-scripts/build-all.sh -p <fvp/soc> -f debian build

**Note:** Follow the ``Host prerequisites for a validated build environment`` from the
`user guide`_ to set up for running the docker environment. (To be done only first time)

The Linux Pure-cap Environment, Debian RootFS, and Debian disk image can also be
built separately depending on the nature of the development user is intended to.
The following commands sequentially lead to build the final Debian disk image.

    ::

        $ cd <morello_workspace>/morello-aarch64/morello
        $ ./scripts/build-all.sh --x86_64
        $ cd -
        $ ./build-scripts/build-debian.sh -p <fvp/soc> -f debian build
        $ ./build-scripts/build-disk-image.sh -p <fvp/soc> -f debian build

The firmware images have to be built and packaged separately in this case. Follow
``Building the Source Code`` section from the `user guide`_ for more details.

Once the build is completed, the morello busybox filesystem with integrated pure-cap
applications is generated at
``<morello_workspace>/morello-aarch64/morello/morello-rootfs``

Running the Linux Pure-cap Environment
======================================

Refer to the section ``Running the Linux Pure-cap Environment (Debian)`` in the
`user guide`_ to know how to launch Morello BusyBox and test the Morello pure-cap
applications for both FVP and SoC.

'morello-aarch64' Development Environment
=========================================

The Linux Pure-cap Environment is embedded within the Debian RootFS as a part
of the ``morello-aarch64`` directory, or better to be termed the `morello-aarch64
Development Environment` as this development environment can be used to build
the Linux Pure-cap Environment on the Morello platform natively.

The morello-aarch64 source along with prebuilt binaries of Morello BusyBox and
pure-cap applications are packed within the Debian disk image. This
morello-aarch64 source provides the scripts to build Morello Busybox, pure-cap
applications, and for generating the final Morello BusyBox filesystem. Also, the
morello-aarch64 source has the prebuilt aarch64 toolchain which is used for
building the morello busybox and pure-cap applications. Musl-libc is built with
libshim disabled as a part of the morello-aarch64 development environment, where
pure-cap applications and busybox packages are linked statically with the object
files (crt1.o, crti.o, crtn.o, libc.a, etc.) from the musl-libc's build artifacts.

The directory structure of the morello-aarch64 development environment on Debian
RootFS is shown below

    ::

        morello-aarch64/
        ├── LICENSE
        ├── morello
        │   ├── bin: host applications binaries for verifying pure-cap application's elf format
        │   ├── env: scripts for exporting URLs of repo and environment variables for the build
        │   ├── examples: prebuilt pure-cap applications and their sources
        │   ├── morello-busybox.tar.gz
        │   ├── morello-rootfs: morello busybox rootfs with C64 busybox packages and pure-cap apps
        │   ├── musl-bin: build artifacts of musl-libc
        │   ├── musl-libc: musl libc sources
        │   ├── projects: morello-busybox sources and Linux kernel headers
        │   ├── scripts: scripts for building musl-libc, pure-cap apps, and morello-busybox.
        │   ├── toolchain: prebuilt aarch64 toolchain for morello platform
        │   └── tools: host applications sources for verifying pure-cap application's elf format
        └── README.md: readme note on the morello-aarch64 development environment

To build the Linux Pure-cap Environment natively, from the booted Debian on the
Morello platform follow the steps below

    ::

        $ cd /morello-aarch64/morello
        $ ./scripts/build-all.sh

**Note**: Testing the native build of the Linux Pure-cap Environment is not
recommended for FVP as it is observed to be very slow to execute on the FVP.
Recommended to exercise it only on SoC.

This will re-generate the Morello BusyBox filesystem with the freshly built pure-cap
applications. On the Debian RootFS, this environment is located at
``/morello-aarch64/morello/morello-rootfs``. The running of this environment
remains the same as mentioned in the previous section.

To get more information on the `morello-aarch64` project refer `morello-aarch64 guide`_

.. _user guide:
 user-guide.rst

.. _morello-aarch64 guide:
 https://git.morello-project.org/morello/morello-aarch64/-/blob/morello/mainline/README.md
