*****************************************
Ubuntu Distribution Installation and Boot
*****************************************

Morello software stack supports the installation and boot of a standard
Ubuntu 20.04 LTS distribution. The distribution is installed on a SATA disk
and since the installed image is persistent it can be used for multiple boots.

**Note:** This distribution does not contain any capability-aware software. It
is a standard AArch64 image to support an environment for future development.

Prerequisites
=============

Before beginning the installation, download the LTS Ubuntu ISO image
of the required Ubuntu version.
The Ubuntu ISO image can be downloaded from `Ubuntu ISO`_.

**For FVP**

Create an empty SATA disk image for installation and boot of the distribution.

::

    dd if=/dev/zero of=ubuntu.satadisk bs=1G count=16

Syncing and building the source code
====================================

Refer to ``Ubuntu Distribution`` section in `user guide`_ to sync and
build the software stack.

Installation
============

**For FVP**

Please refer to `Arm Ecosystem FVPs`_ to download the Morello FVP.
Run the FVP model with the following parameters for the installation:

::

    ./run-scripts/run_model.sh -m <model binary path> -f distro -d <sata disk path> -i <installer image path>

**For Development Board**

Two mass storage devices are required: a USB drive to flash the installer
(downloaded LTS Ubuntu ISO image) and a SATA drive to install the Ubuntu.
The Morello board already has an empty SATA drive connected and secured in an
ATX case.

Connect USB drive to the host machine. Use the following commands to
prepare the image on a USB drive:

        ::

             $ lsblk
             $ sudo dd if=<Ubuntu image name> of=/dev/sdX conv=fsync bs=1M
             $ sync

Note: Replace ``/dev/sdX`` with the handle corresponding to your USB drive
as identified by the ``lsblk`` command.

Insert the bootable installer disk created earlier (USB drive) to one of the
four USB3 ports in the backside of the Morello board.

Reboot the board by issuing the following command
in the MCC console:

    ::

             Cmd> REBOOT

Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc key within
the 10s UEFI boot timeout. Enter the UEFI Boot Manager
menu and select the disk (USB drive) with the installer.
Select the appropriate option from the installer.
Once the installation is complete, remove the disk (USB drive) which
contains the installer.

Boot
====

**For FVP**

Run the FVP model with the following parameters to boot the distribution which
is installed on the SATA disk:

::

    ./run-scripts/run_model.sh -m <model binary path> -f distro -d <sata disk path>

**For Development Board**

Reboot the board by issuing the following command
in the MCC console:

    ::

             Cmd> REBOOT

Switch to the AP UART console. Once the application core has started
booting the UEFI firmware, enter the UEFI menu by pressing the Esc key within
the 10s UEFI boot timeout. Enter the UEFI Boot Manager menu and select the
Ubuntu-installed disk (SATA drive).

The system will boot into an Ubuntu environment.

.. _Ubuntu ISO:
 https://ubuntu.com/download/server/arm

.. _user guide:
 user-guide.rst

.. _Arm Ecosystem FVPs:
 https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
