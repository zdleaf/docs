***********************************
LLVM toolchain with Morello support
***********************************

Introduction
============

The Morello LLVM implementation is based on the CHERI-LLVM-fork_.
Support for Morello and the pure capability ABI has been added to clang, llvm, lld,
lldb, libcxx, libcxxabi and compiler-rt.

Getting a toolchain
===================

There are two ways of obtaining a toolchain:

- Download pre-built binaries.
- Build the toolchain from source. Note that at the moment the bare-metal toolchain cannot be built from source.

Pre-built binaries
------------------

Pre-built binaries are hosted at MorelloLLVMBinaries_. Binaries are available on the following branches:

  - morello/release-X.Y: Contains pre-builts for the android toolchain X.Y release.
  - morello/baremetal-release-X.Y: Contains prebuilds for the bare-metal toolchain X.Y release.

where X.Y corresponds to the release version.

Building from source
--------------------

The source code is at MorelloLLVMCode_, with the following
branches:

- morello/master: Contains Morello support and this is the development branch
- morello/release-X.Y: Release branch for the X.Y release.

See CMakeBuildInstructions_ on instructions and requirements for how to build LLVM from source code.

Building the android toolchain from source
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: sh

  mkdir repo && cd repo
  repo init -u https://git.morello-project.org/morello/manifest.git -b morello/mainline -g toolchain-src
  repo sync
  toolchain-src/toolchain/llvm_android/build.py --no-build windows

To build a toolchain specific to a X.Y release branch replace ``-b morello/mainline`` with ``-b morello/release-X.Y`` in
the ``repo init`` command.

Building the tools using cmake
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: sh

  > CC=<path-to-host-clang> \
    CXX=<path-to-host-clang++> \
    cmake \
      -DLLVM_TARGETS_TO_BUILD:STRING="X86;AArch64" \
      -DCMAKE_BUILD_TYPE:STRING=Release \
      -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON \
      -DBUILD_SHARED_LIBS:BOOL=ON \
      -DCMAKE_SKIP_BUILD_RPATH:BOOL=OFF \
      -DCMAKE_INSTALL_RPATH:STRING=\$ORIGIN/../lib \
      -DLLVM_ENABLE_ASSERTIONS:BOOL=ON \
      -DCMAKE_INSTALL_RPATH:STRING=\$ORIGIN/../lib \
      -DCMAKE_BUILD_WITH_INSTALL_RPATH:BOOL=ON \
      -DLLVM_ENABLE_LIBCXX:BOOL=OFF \
      -DLIBUNWIND_ENABLE_THREADS:BOOL=ON \
      -DCMAKE_INSTALL_PREFIX:STRING=<path-to-install-location> \
      -DLLVM_ENABLE_EH:BOOL=ON \
      -DLLVM_ENABLE_RTTI:BOOL=ON \
      -DLLVM_ENABLE_Z3_SOLVER:BOOL=OFF \
      -DLIBCXX_CXX_ABI:STRING=libcxxabi \
      -DLIBCXX_CXX_ABI_INCLUDE_PATHS:STRING=<path-to-llvm-project>/libcxxabi/include \
      -DLIBCXXABI_USE_LLVM_UNWINDER:BOOL=ON \
      -DLIBCXX_ENABLE_THREADS:BOOL=ON \
      -DLIBCXXABI_ENABLE_THREADS:BOOL=ON \
      -DLLVM_ENABLE_PROJECTS="clang;lld;lldb;compiler-rt;libunwind;libcxx;libcxxabi" \
      <path-to-llvm-project>/llvm
  >  make -j16
  >  make install

Documentation
=============

User guide
----------

This document gives an overview of the available Morello-aware tools (compiler,
assembler, disassembler, linker, debugger) and highlights the various command
line options that need to be added in order to use the Morello-specific functionality.

The user guide can be found at MorelloLLVMBinaries_ in the morello/release-docs-X.Y branch, where X.Y
is the release version. The document for the current release is User-Guide_.

Bare-metal user guide
---------------------

This document shows how to install and use the LLVM compiler with Morello support for bare-metal.
It describes how to produce baremetal images, how to run these on the FVP,
along with information on the baremetal environment provided by the toolchain.

The bare-metal user guide can be found at MorelloLLVMBinaries_ in the morello/release-docs-X.Y branch,
where X.Y is the release version. The document for the current release is User-Guide-Baremetal_.

Morello specifications
----------------------

Version 1.5 of the toolchain implements the following specifications: PCS_, DWARF_, ELF_, ACLE_.
To contribute to these documents use ABI-GitHub_ for the PCS, ELF and DWARF specifications and
ACLE-GitHub_ for the ACLE.

CHERI documentation
-------------------

CHERI-Pure-capability-guide_ is a brief introduction to the CHERI C/C++ programming languages.
A guide for the CHERI hybrid programming model is expected to be published soon.
See CHERI-publications_ for a more extensive list of CHERI literature.

Mailing lists
=============

For questions and further discussions see the llvm-morello@op-lists.linaro.org mailing list.
You can subscribe to this list or view the archives at llvm-morello_.

Known issues
============

The following is a list of known bugs. For a list of known missing features see the User-Guide_ document.

- The 'C' constraint should be used for capability operands in inline assembly statements. Using the
  wrong constraint can cause an internal compiler error.
- Capability relocations on symbols with size 0 will produce a warning. This warning can be
  ignored for capabilities to executable code, as the bounds of the resulting capabilities
  are not a function of the symbol size.
- Launching a process on the FVP can be slow, causing lldb to timeout in
  ``process launch``. This manifests as 'packet A returned an error' messages. To
  work around the issue, you should add the following command to your scripts or
  to lldb init: ``settings set plugin.process.gdb-remote.packet-timeout 300``.

.. _llvm-morello: https://op-lists.linaro.org/mailman/listinfo/llvm-morello
.. _User-Guide: https://git.morello-project.org/morello/llvm-project-releases/-/blob/morello/release-docs-1.5/UserGuide.pdf
.. _User-Guide-Baremetal: https://git.morello-project.org/morello/llvm-project-releases/-/blob/morello/release-docs-1.5/UserGuideBareMetal.pdf
.. _PCS: https://developer.arm.com/documentation/102205/latest
.. _DWARF: https://developer.arm.com/documentation/102215/latest
.. _ELF: https://developer.arm.com/documentation/102072/latest
.. _ACLE: https://developer.arm.com/documentation/102273/latest
.. _ABI-GitHub: https://github.com/ARM-software/abi-aa
.. _ACLE-GitHub: https://github.com/ARM-software/acle
.. _CHERI-LLVM-fork: https://github.com/CTSRD-CHERI/llvm-project
.. _CHERI-Pure-capability-guide: https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-947.pdf
.. _CHERI-publications: https://www.cl.cam.ac.uk/research/security/ctsrd/cheri/cheri-publications.html
.. _MorelloLLVMBinaries: https://git.morello-project.org/morello/llvm-project-releases
.. _MorelloLLVMCode: https://git.morello-project.org/morello/llvm-project
.. _CMakeBuildInstructions: https://llvm.org/docs/CMake.html
