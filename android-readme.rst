***************************
The Android/Morello release
***************************

A. Introduction
===============

Welcome to the Android/Morello code base.

The project is aimed at introducing everyone to the world of capability-aware
software. The document is a guide on what has been done already, on how to try
Android/Morello, and on potential future directions of the research.

Morello is a prototype architecture whose only hardware implementation is the
associated prototype evaluation board. The aim of this release is to provide
a basis for future research, and as such it might contain patches that aren't
suitable for production environments. For example, patches that introduce
configurations switching off fixes for certain security vulnerabilities in order
to demonstrate that Morello still efficiently prevents the related classes of
security vulnerabilities. Any future capability-aware software stack reusing
patches contained in this release should apply great care to ensure that those
patches are suitable for the targeted environment.

This release is based on Android R (11.0.0_r27) and provides different
device profiles:

#. :code:`morello_nano`, a minimal system Android profile booting
   the system to console shell.
#. :code:`morello_swr`, a full Android profile booting the system
   to the Home screen, using software rendering (no hardware acceleration).

In this release, only a small subset of Android components are ported to
Morello Pure-cap ABI [B]. There are plans to extend the Pure-cap ABI support
further. Please, contribute source code changes and ideas on use-cases that
you find useful.

The ported Android components include:

- the standard C library (Bionic)
- the logd, servicemanager services and the second stage init
- libjpeg-turbo (the "tjbench" and other workloads + demonstration of how
  Morello prevents heap-based buffer over-read CVE-2018-19664)
- libpcre2 (the "pcre2test" and "pcre2grep" workloads)
- libpdfium (the "pdfium_test" workload)
- libpng (the "pngtest" workload)
- zlib (the "zlib_bench" and "minigzip" workloads)
- BoringSSL (the "boringssl_ssl_test" and "boringssl_crypto_test" workloads)

Additionally, this release provides:

- core Morello support in the kernel
- basic demo of capability-based compartmentalization

The Morello architecture does not include AArch32, and therefore no support for
AArch32 is included in this release.

B. Hybrid-cap and Pure-cap ABIs
===============================

Morello provides support of two ABIs: Hybrid-cap and Pure-cap.

**Hybrid-cap** (Hybrid-capability)
 Pointers are addresses (64-bit values, as in the Base - usual AArch64 - ABI)
 unless explicitly declared as capabilities.

**Pure-cap** (Pure-capability)
 Pointers are always 128+1-bit capabilities [F.1].

When this document says that an Android software component was ported to
Morello, that means it was changed (where necessary) to support being compiled
for the Pure-cap ABI. Ports of system libraries and workloads are available in
both the Hybrid-cap and the Pure-cap ABIs, while ported services are only built
in the Pure-cap ABIs.

Android software components that haven't been ported to Morello for this release
are being built only in the Hybrid-cap ABI. We choose to use the Pure-cap ABI
for Morello ports since it introduces capability-based features without explicit
variable annotations, which would have been required when porting in Hybrid-cap
ABI. Unless changed to explicitly use capabilities, the software built in the
Hybrid-cap behaves as if it was compiled for the Base ABI. Since effectively the
software components behave the same in the Hybrid-cap and Base ABIs, sometimes
their ABI is referred to as the Base ABI.


C. Morello ports and demonstrations
===================================

1. Bionic
---------

Source: `bionic`_

Porting Bionic is a prerequisite for porting anything else in the Android
user-space, since Bionic is the standard C library and provides application
startup as well as the most common interfaces like memcpy, fopen, sprintf and
many more of those.

Support for the Pure-cap ABI required some changes to `bionic`_ libc. This
section is a high level overview of these changes. For full details please
consult Bionic's git history and the respective code snippets.

An important dependency of Bionic, which also was ported to Pure-cap is the
`jemalloc`_ memory allocator. The allocator interface sets tight bounds in
capabilities it returns which is important for preventing heap spatial
safety violations.

1.1. libshim translation layer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The major change to Bionic is that system calls are no longer generated as
usual for both the Base / Hybrid-cap and Pure-cap ABIs. Instead, there is a
thin system call translation layer (`libshim`_) introduced which enables
operation of the Pure-cap ABI executables while the kernel only supports Base
ABI. This is providing only limited emulation of kernel Pure-cap ABI support
and is a temporary stage only - until the Pure-cap ABI is supported in the
kernel (please, see also section [C.7])

Additionally, libshim converts the command line arguments, environment and
auxiliary vectors upon the process startup to comply with the expected memory
layout in the Pure-cap ABI, because the original layout is prepared by the
kernel which currently provides the Base ABI even for Pure-cap user-space.

1.2. Capability reconstruction at process startup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

While in the Base ABI pointers are just addresses, in the Pure-cap ABI the
pointers are required to be capabilities. This includes the pointers in global
data section. In the Base ABI, these values would be hard-coded into the binary.
However capabilities are intrinsically dynamic values which have to be
constructed following architecturally-defined rules in runtime. For the Pure-cap
ABI, if the binaries are linked dynamically, the dynamic relocations are
sufficient to describe the values to be derived. For statically linked
binaries, the executable is extended with a table of capability descriptors
(__cap_relocs ELF section), which are used during the process initialization to
derive the required capability values. This is done by
:code:`__morello_init_static` (`morello_init_static.S`_) which is invoked by
:code:`_start` (`crtbegin.c`_).

1.3. Extended set of string-processing routines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Basic string routines - memcpy, mempcpy, memset, memmove, memcmp - take
pointers, which means capabilities in the Pure-cap ABI and addresses in the
Hybrid-cap ABI. In the Hybrid-cap ABI, applications might need to use the
functions on buffers referred to through explicitly declared capabilities. For
this purpose, the standard string routines are complemented with counterparts
accepting capabilities: memcpy_c, mempcpy_c, memset_c, memmove_c, memcmp_c.

For memcpy, mempcpy, memmove both of the versions are implemented in a way that
guarantees the capability tags are preserved when copying memory content.

1.4. Dynamic linkage
^^^^^^^^^^^^^^^^^^^^

When dynamically linking, capability initialization is done by the dynamic
linker as part of processing the dynamic Morello relocations. Each dynamic
Morello relocation provides information necessary in order to figure out the
corresponding capability bounds and permissions. Currently, dynamic Morello
relocations are issued and handled only for the Pure-cap ABI binaries.

In the Pure-cap ABI, the toolchain might have to emit Morello relocations even
when no relocation is needed in the Base ABI. This is due to the requirement
that capabilities used for accessing variables must be derived from other
capabilities at runtime.

For thread-local storage (TLS) variables, Morello defines the relocations
needed to implement the descriptor-based thread-local storage (TLSDESC) dynamic
relocations. The TLSDESC Morello resolvers are different from the Base ABI
resolvers because they return capabilities instead of offsets from the thread
pointer. They are implemented here: `tlsdesc_resolver.S`_

For more details on the Morello dynamic relocations codes, operations, and
relocation fragment formats, please, see:
`Morello Supplement to ELF for the Arm 64-bit Architecture`_.

1.5. Other changes
^^^^^^^^^^^^^^^^^^

Additional changes in Bionic are mostly related to the manipulation of pointer
values. Details on the particular changes are provided in the relevant commit
messages. The common changes required for porting to Morello are summarised in
the section [D] below.

1.6. Unit tests
^^^^^^^^^^^^^^^

Bionic's unit tests (:code:`bionic-unit-tests` /
:code:`bionic-unit-tests-static`), which are built both for the Hybrid-cap and
Pure-cap ABIs, are located at the following paths in the output file system:

.. code-block:: sh

  /data/nativetest64/bionic-unit-tests/bionic-unit-tests
  /data/nativetest64/bionic-unit-tests-static/bionic-unit-tests-static

for the Hybrid-cap ABI, and

.. code-block:: sh

  /data/nativetestc64/bionic-unit-tests/bionic-unit-tests
  /data/nativetestc64/bionic-unit-tests-static/bionic-unit-tests-static

for the Pure-cap ABI.

The Bionic test suite is extended to test the core Morello support in the
kernel. The kernel tests are run in the Hybrid-cap ABI only.

2. servicemanager
-----------------

Source: `servicemanager`_

servicemanager maintains a directory of Android system services and
provides a mapping between the interface name and the Binder handle of the
corresponding service.

In Android/Morello servicemanager is only built for and operates in the Pure-cap
ABI.

While servicemanager has no specific unit tests on its own, the following binder
unit tests are exercising the servicemanager functionality:

- binderLibTest;
- binderThroughputTest;
- binderSafeInterfaceTest.

Note: only the Hybrid-cap ABI is built for these targets, the Pure-cap ABI is
not currently supported for the tests - it is possible for them to exercise
the Pure-cap servicemanager since they are using the Binder-based IPC for that.

Locations in the output file system:

.. code-block:: sh

  /data/nativetest64/binderLibTest/binderLibTest
  /data/nativetest64/binderThroughputTest/binderThroughputTest
  /data/nativetest64/binderSafeInterfaceTest/binderSafeInterfaceTest

3. logd service
---------------

Source: `logd`_

logd is the Android logging daemon.

In Android/Morello logd is only built for and operates in the Pure-cap ABI.

logd can be exercised with the following workloads which are built both for the
Hybrid-cap and Pure-cap ABIs:

- logd-unit-tests
- liblog-unit-tests
- liblog-benchmarks

Locations in the output file system:

.. code-block:: sh

  /data/nativetest64/logd-unit-tests/logd-unit-tests
  /data/nativetest64/liblog-unit-tests/liblog-unit-tests
  /data/benchmarktest64/liblog-benchmarks/liblog-benchmarks

for the Hybrid-cap ABI, and

.. code-block:: sh

  /data/nativetestc64/logd-unit-tests/logd-unit-tests
  /data/nativetestc64/liblog-unit-tests/liblog-unit-tests
  /data/benchmarktestc64/liblog-benchmarks/liblog-benchmarks

for the Pure-cap ABI.

Please, note that some of the unit tests in logd-unit-tests assume certain state
of the logd buffers - specifically on certain logd buffers being non-empty.

4. Android init (second stage)
------------------------------

Source: `init`_

init is the Android service responsible for early init boot sequence.
The boot sequence is split into first stage init, SELinux setup and second stage init.

The second stage init has been switched to operate in the Pure-cap ABI.

5. Workloads
------------

Among other components, a few workloads have been ported to the Pure-cap ABI.

5.1. libjpeg-turbo ("tjbench" and others)
-----------------------------------------

Source: `libjpeg_turbo`_

Workloads: tjbench, turbojpeg, djpeg, cjpeg, tjunittest, jpegtran, md5.

Locations in the output file system:

.. code-block:: sh

  /system/bin/tj64
  /system/bin/dj64
  /system/bin/cj64
  /system/bin/tjunit64
  /system/bin/jpegtran64
  /system/bin/md5cmp64

for the Hybrid-cap ABI, and

.. code-block:: sh

  /system/bin/tjc64
  /system/bin/djc64
  /system/bin/cjc64
  /system/bin/tjunitc64
  /system/bin/jpegtranc64
  /system/bin/md5cmpc64

for the Pure-cap ABI.

5.1.1 libjpeg-turbo CVE-2018-19664 prevention demo
--------------------------------------------------

The libjpeg-turbo port has been extended with additional custom build targets,
including one with a disabled fix for the heap-based buffer over-read CVE-2018-19664
vulnerability in order to demonstrate detection and prevention of memory safety
violation. This is an example of how Morello and its accompanying software
modifications can significantly increase memory protection.

More details are provided in this document: `libjpeg_turbo_cve_prevention_demo`_

5.2. libpdfium ("pdfium_test")
------------------------------

Source: `pdfium`_

Locations in the output file system:

.. code-block:: sh

  /system/bin/pdfium_test

for the Hybrid-cap ABI, and

.. code-block:: sh

  /system/bin/pdfium_test-c64

for the Pure-cap ABI.

5.3. libpng ("pngtest")
-----------------------

Source: `libpng`_

Locations in the output file system:

.. code-block:: sh

  /data/nativetest64/pngtest/pngtest

for the Hybrid-cap ABI, and

.. code-block:: sh

  /data/nativetestc64/pngtest/pngtest

for the Pure-cap ABI.

5.4. zlib ("zlib_bench" and "minigzip")
---------------------------------------

Source: `zlib`_

Locations in the output file system:

.. code-block:: sh

  /system/bin/minigzip
  /system/bin/zlib_bench

for the Hybrid-cap ABI, and

.. code-block:: sh

  /system/bin/minigzip-c64
  /system/bin/zlib_bench-c64

for the Pure-cap ABI.

5.5. BoringSSL ("boringssl_ssl_test" and "boringssl_crypto_test")
-----------------------------------------------------------------

Source: `boringssl`_

Locations in the output file system:

.. code-block:: sh

  /data/nativetest64/boringssl_crypto_test/boringssl_crypto_test
  /data/nativetest64/boringssl_ssl_test/boringssl_ssl_test

for the Hybrid-cap ABI, and

.. code-block:: sh

  /data/nativetestc64/boringssl_crypto_test/boringssl_crypto_test
  /data/nativetestc64/boringssl_ssl_test/boringssl_ssl_test

for the Pure-cap ABI.

The assembly implementation of BoringSSL routines hasn't been ported
to Morello, so OPENSSL_NO_ASM preprocessor flag is defined when
building for the Pure-cap ABI.

5.6. libpcre2
----------------------------

Source: `pcre`_

PCRE2 is a Perl-compatible regular expression library.

It has been ported to the Pure-cap ABI and can be exercised with the following workloads
which are built both for the Hybrid-cap and Pure-cap ABIs:
 - pcre2test
 - pcre2grep

Locations in the output file system:

.. code-block:: sh

  /data/nativetest64/pcre2test/pcre2grep
  /data/nativetest64/pcre2test/pcre2test

for the Hybrid-cap ABI, and

.. code-block:: sh

  /data/nativetestc64/pcre2test/pcre2grep
  /data/nativetestc64/pcre2test/pcre2test

for the Pure-cap ABI.

6. Basic demo of capability-based compartmentalization
------------------------------------------------------

Source: `Compartment Demo`_

One of the features provided by capability architectures is an alternative way
to implement software compartmentalization. There are many ways to implement
this using Morello and many possible use-cases.

This is a simple demo aimed at demonstrating the principles of
compartmentalization in Morello. It provides a limited framework that allows
multiple compartments to run in the same address space, supported by a
privileged component, the compartment manager.

For more details, please, see: `Compartment Demo Documentation`_

Please, contribute code extending this or providing new demonstrations, sharing
use-cases.

7. Kernel
---------

Source: Kernel_

Like any other architecture extension, Morello requires a number of kernel
changes to enable the use of Morello functionalities in userspace. As part of
this release, a Linux kernel tree with "core" support for Morello is
provided. This core support can be summarized as:

- Runtime feature detection and system initialization.
- Handling of Morello registers (initialization, context-switching, etc.), with
  special care for capability registers that extend standard AArch64 64-bit
  registers.
- Access to capabilities in memory.
- Handling of capability faults.
- Minimal support for the new C64 instruction set.
- Basic ptrace requests for accessing the Morello state.

Fore more information, see: `Kernel Morello documentation`_.

As mentioned previously, this core Morello support does not modify the existing
arm64 kernel-user ABI. This means that backwards-compatibility is preserved, but
also that there is **no support for the Pure-cap ABI** enabled, which is why
a userspace shim is required (see [C.1.1]). Investigations are underway to remove
this requirement once sufficient support for a new Pure-cap kernel-user ABI is
implemented.

D. General guidelines for porting to Morello
============================================

The main aspects [F.2] that should be considered when porting software to the
Morello Pure-cap ABI:

- with respect to the memory layout, pointers are 128-bit and must be aligned on
  their size;
- each pointer is in fact a capability i.e. it carries information on the bounds
  of the memory region that can be accessed using the pointer as well as
  permissions on which accesses are allowed;
- there are some restrictions related to arithmetic/bitwise operations on
  pointers.

In some cases, changes are required only for Pure-cap support and have unwanted
effects in Hybrid-cap (such as increased memory consumption). In these cases,
__CHERI_PURE_CAPABILITY__ macro is used:

.. code-block:: cpp

  #ifdef __CHERI_PURE_CAPABILITY__
    <specific code for Pure-cap>
  #else /* !__CHERI_PURE_CAPABILITY__ */
    <original code>
  #endif /* !__CHERI_PURE_CAPABILITY__ */


MORELLO-META mark-up
--------------------

MORELLO-META markup is now deprecated.


E. Ideas on future work
=======================

The initial Morello release is providing minimal support of capabilities and a
few Morello ports. There is plenty of room for further development, research and
reflection on the results.

This section describes potential future directions within the project.

In short-term:

- moving towards compatibility with defined kernel Pure-cap ABI interface
- support kernel Pure-cap interface in Bionic without libshim

Ideas for long-term:

- porting more workloads for benchmarking Pure-cap
- support stack unwinding and C++ exceptions in the Pure-cap ABI
- research of use-cases for and schemes of compartmentalization
- support capabilities in graphics stack once it is enabled
- Pure-cap port of Android Runtime (ART) and Zygote
- many more.

Contributions are very welcome!


F. References
=============

- [1] https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-941.pdf
- [2] https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-947.pdf


.. _bionic:
  https://git.morello-project.org/morello/android/platform/bionic/-/tree/morello/android11-qpr1-release
.. _libshim:
  https://git.morello-project.org/morello/android/platform/external/libshim/-/blob/morello/mainline/README.rst
.. _jemalloc:
  https://git.morello-project.org/morello/android/platform/external/jemalloc_new/-/tree/morello/android11-qpr1-release
.. _crtbegin.c:
  https://git.morello-project.org/morello/android/platform/bionic/-/blob/morello/android11-qpr1-release/libc/arch-common/bionic/crtbegin.c
.. _morello_init_static.S:
  https://git.morello-project.org/morello/android/platform/bionic/-/blob/morello/android11-qpr1-release/libc/arch-morello/bionic/__morello_init_static.S
.. _tlsdesc_resolver.S:
  https://git.morello-project.org/morello/android/platform/bionic/-/blob/morello/android11-qpr1-release/linker/arch/morello/tlsdesc_resolver.S
.. _servicemanager:
  https://git.morello-project.org/morello/android/platform/frameworks/native/-/tree/morello/android11-qpr1-release/cmds/servicemanager
.. _logd:
  https://git.morello-project.org/morello/android/platform/system/core/-/tree/morello/android11-qpr1-release/logd
.. _init:
  https://git.morello-project.org/morello/android/platform/system/core/-/tree/morello/android11-qpr1-release/init
.. _libjpeg_turbo:
  https://git.morello-project.org/morello/android/platform/external/libjpeg-turbo/-/tree/morello/android11-qpr1-release
.. _libjpeg_turbo_cve_prevention_demo:
  https://git.morello-project.org/morello/android/platform/external/libjpeg-turbo/-/blob/morello/android11-qpr1-release/cve-2018-19664-demo.rst
.. _pdfium:
  https://git.morello-project.org/morello/android/platform/external/pdfium/-/tree/morello/android11-qpr1-release
.. _libpng:
  https://git.morello-project.org/morello/android/platform/external/libpng/-/tree/morello/android11-qpr1-release
.. _zlib:
  https://git.morello-project.org/morello/android/platform/external/zlib/-/tree/morello/android11-qpr1-release
.. _boringssl:
  https://git.morello-project.org/morello/android/platform/external/boringssl/-/tree/morello/android11-qpr1-release
.. _pcre:
  https://git.morello-project.org/morello/android/platform/external/pcre/-/tree/morello/android11-qpr1-release
.. _Compartment Demo:
  https://git.morello-project.org/morello/android/vendor/arm/morello-examples/-/tree/morello/mainline/compartment-demo
.. _Compartment Demo Documentation:
  https://git.morello-project.org/morello/android/vendor/arm/morello-examples/-/blob/morello/mainline/compartment-demo/README.rst
.. _Kernel:
  https://git.morello-project.org/morello/kernel/linux/-/tree/morello/master
.. _Kernel Morello documentation:
  https://git.morello-project.org/morello/kernel/linux/-/blob/morello/master/Documentation/arm64/morello.rst
.. _Morello Supplement to ELF for the Arm 64-bit Architecture:
  https://developer.arm.com/documentation/102072/latest
