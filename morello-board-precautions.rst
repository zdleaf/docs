********************************************************
Morello System Development Platform specific Precautions
********************************************************

- The Morello System Development Platform is intended for use within a
  laboratory or engineering development environment. Do not use the
  Morello System Development Platform near equipment that is sensitive to
  electromagnetic emissions, for example, medical equipment.
- Never subject the board to high electrostatic potentials.
  Observe Electrostatic Discharge (ESD) precautions when handling any board.

  - Always wear a grounding strap when handling the board.
  - Avoid touching the component pins or any other metallic elements.
