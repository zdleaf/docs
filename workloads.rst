**************************
Tests Suites and Workloads
**************************

This document explains how build individual tests and
run on Morello Fixed Virtual Platform (Morello FVP)

Android tests suites and workloads - build and run
==================================================

Prerequisites:

On the host machine, after having built Android following the instructions
in the "Android Stack" section of the `user guide`_, and started FVP as
per the "Running the software on FVP" section:

::

        cd <morello_workspace>/android/
        source build/envsetup.sh
        lunch morello_nano-eng   # or morello_swr-eng
        m <test name>
        adb push <source path> <destination path> //please refer to individual test case build and run steps

All tests should be run as root and may fail otherwise. There are two possible options:

- On the console, ``su`` must be run to become root (the default login user is ``shell``).
- When using ``adb shell``, the login user is already root and no additional step is required.

Bionic unit tests
-----------------

Steps to build and run individual tests:

Build:

.. code-block:: sh

 m bionic-unit-tests

 for BUILD_SUFFIX in 64 c64; do
   adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/bionic-unit-tests/bionic-unit-tests  \
     /data/nativetest${BUILD_SUFFIX}/bionic-unit-tests/bionic-unit-tests
   adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/bionic-loader-test-libs \
     /data/nativetest${BUILD_SUFFIX}/
 done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

 for BUILD_SUFFIX in 64 c64; do
   /data/nativetest${BUILD_SUFFIX}/bionic-unit-tests/bionic-unit-tests
 done

Static Bionic unit tests
------------------------

Steps to build and run individual tests:

Build:

.. code-block:: sh

 m bionic-unit-tests-static

 for BUILD_SUFFIX in 64 c64; do
   adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/bionic-unit-tests-static/bionic-unit-tests-static  \
     /data/nativetest${BUILD_SUFFIX}/bionic-unit-tests-static/bionic-unit-tests-static
 done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

 for BUILD_SUFFIX in 64 c64; do
   /data/nativetest${BUILD_SUFFIX}/bionic-unit-tests-static/bionic-unit-tests-static
 done

servicemanager
--------------

Steps to build and run individual tests:

binderDriverInterfaceTest
^^^^^^^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

   m binderDriverInterfaceTest

   for BUILD_SUFFIX in 64 c64; do
     adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/binderDriverInterfaceTest/binderDriverInterfaceTest \
       /data/nativetest${BUILD_SUFFIX}/binderDriverInterfaceTest/binderDriverInterfaceTest
   done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

   for BUILD_SUFFIX in 64 c64; do
     /data/nativetest${BUILD_SUFFIX}/binderDriverInterfaceTest/binderDriverInterfaceTest
   done

binderLibTest
^^^^^^^^^^^^^

Build

.. code-block:: sh

    m binderLibTest

    for BUILD_SUFFIX in 64 c64; do
      adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/binderLibTest/binderLibTest \
        /data/nativetest${BUILD_SUFFIX}/binderLibTest/binderLibTest
    done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    for BUILD_SUFFIX in 64 c64; do
      /data/nativetest${BUILD_SUFFIX}/binderLibTest/binderLibTest
    done


binderSafeInterfaceTest
^^^^^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

    m binderSafeInterfaceTest
    adb push out/target/product/morello/data/nativetest64/binderSafeInterfaceTest/binderSafeInterfaceTest \
      /data/nativetest64/binderSafeInterfaceTest/binderSafeInterfaceTest

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

     ( ulimit -n 128 && /data/nativetest64/binderSafeInterfaceTest/binderSafeInterfaceTest )


binderTextOutputTest
^^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

    m binderTextOutputTest

    for BUILD_SUFFIX in 64 c64; do
      adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/binderTextOutputTest/binderTextOutputTest \
        /data/nativetest${BUILD_SUFFIX}/binderTextOutputTest/binderTextOutputTest
    done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    for BUILD_SUFFIX in 64 c64; do
      /data/nativetest${BUILD_SUFFIX}/binderTextOutputTest/binderTextOutputTest
    done


binderThroughputTest
^^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

    m binderThroughputTest

    for BUILD_SUFFIX in 64 c64; do
      adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/binderThroughputTest/binderThroughputTest \
        /data/nativetest${BUILD_SUFFIX}/binderThroughputTest/binderThroughputTest
    done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    for BUILD_SUFFIX in 64 c64; do
     /data/nativetest${BUILD_SUFFIX}/binderThroughputTest/binderThroughputTest -i 1000 -p
    done


binderStabilityTest
^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

    m binderStabilityTest

    for BUILD_SUFFIX in 64 c64; do
      adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/binderStabilityTest/binderStabilityTest \
        /data/nativetest${BUILD_SUFFIX}/binderStabilityTest/binderStabilityTest

      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libbinder_ndk.so \
        /data/nativetest${BUILD_SUFFIX}/binderStabilityTest/
      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libandroid_runtime_lazy.so \
        /data/nativetest${BUILD_SUFFIX}/binderStabilityTest/
    done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    for BUILD_SUFFIX in 64 c64; do
      LD_LIBRARY_PATH=/data/nativetest${BUILD_SUFFIX}/binderStabilityTest \
        /data/nativetest${BUILD_SUFFIX}/binderStabilityTest/binderStabilityTest
    done

logd service
------------
Steps to build and run individual tests:

liblog-unit-tests
^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

   m liblog-unit-tests

   for BUILD_SUFFIX in 64 c64; do
     adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/liblog-unit-tests/liblog-unit-tests \
       /data/nativetest${BUILD_SUFFIX}/liblog-unit-tests/liblog-unit-tests
   done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

  for BUILD_SUFFIX in 64 c64; do
    /data/nativetest${BUILD_SUFFIX}/liblog-unit-tests/liblog-unit-tests
  done


logd-unit-tests
^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

  m logd-unit-tests

  for BUILD_SUFFIX in 64 c64; do
    adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/logd-unit-tests/logd-unit-tests \
      /data/nativetest${BUILD_SUFFIX}/logd-unit-tests/logd-unit-tests
  done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

  for BUILD_SUFFIX in 64 c64; do
    /data/nativetest${BUILD_SUFFIX}/logd-unit-tests/logd-unit-tests
  done


liblog-benchmarks
^^^^^^^^^^^^^^^^^

Note: This is not an actual benchmark when run on FVP

Build:

.. code-block:: sh

  m liblog-benchmarks

  for BUILD_SUFFIX in 64 c64; do
    adb push out/target/product/morello/data/benchmarktest${BUILD_SUFFIX}/liblog-benchmarks/liblog-benchmarks \
      /data/benchmarktest${BUILD_SUFFIX}/liblog-benchmarks/liblog-benchmarks
  done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

  for BUILD_SUFFIX in 64 c64; do
    /data/benchmarktest${BUILD_SUFFIX}/liblog-benchmarks/liblog-benchmarks  \
      --benchmark_repetitions=1 --benchmark_min_time=0.001
  done


libjpeg-turbo ("tjbench" and others)
------------------------------------

Build:

.. code-block:: sh

    m tjbench djpeg cjpeg tjunittest jpegtran md5

    adb shell mkdir -p /data/local/tmp/system/bin
    adb shell mkdir -p /data/local/tmp/libjpeg-turbo-images

    for BUILD_SUFFIX in 64 c64; do
      adb push out/target/product/morello/system/bin/tj${BUILD_SUFFIX} /data/local/tmp/system/bin
      adb push out/target/product/morello/system/bin/dj${BUILD_SUFFIX} /data/local/tmp/system/bin
      adb push out/target/product/morello/system/bin/cj${BUILD_SUFFIX} /data/local/tmp/system/bin
      adb push out/target/product/morello/system/bin/tjunit${BUILD_SUFFIX} /data/local/tmp/system/bin
      adb push out/target/product/morello/system/bin/jpegtran${BUILD_SUFFIX} /data/local/tmp/system/bin
      adb push out/target/product/morello/system/bin/md5cmp${BUILD_SUFFIX} /data/local/tmp/system/bin
    done

    adb push external/libjpeg-turbo/testimages/* \
      /data/local/tmp/libjpeg-turbo-images

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    for BUILD_SUFFIX in 64 c64; do
      cd /data/local/tmp && rm -rf tjbench_tile && mkdir tjbench_tile && cd tjbench_tile && \
        cp /data/local/tmp/libjpeg-turbo-images/testorig.ppm testout_tile.ppm && \
        /data/local/tmp/system/bin/tj${BUILD_SUFFIX} testout_tile.ppm 95 -rgb -quiet -tile -benchtime 0.01 -warmup 0 && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_8x8.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 847fceab15c5b7b911cb986cf0f71de3 testout_tile_420_Q95_8x8.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} d83dacd9fc73b0a6f10c09acad64eb1e testout_tile_422_Q95_8x8.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_8x8.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_16x16.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} ca45552a93687e078f7137cc4126a7b0 testout_tile_420_Q95_16x16.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 35077fb610d72dd743b1eb0cbcfe10fb testout_tile_422_Q95_16x16.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_16x16.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_32x32.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} d8676f1d6b68df358353bba9844f4a00 testout_tile_420_Q95_32x32.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} e6902ed8a449ecc0f0d6f2bf945f65f7 testout_tile_422_Q95_32x32.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_32x32.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_64x64.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 4e4c1a3d7ea4bace4f868bcbe83b7050 testout_tile_420_Q95_64x64.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 2b4502a8f316cedbde1da7bce3d2231e testout_tile_422_Q95_64x64.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_64x64.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_128x128.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} f24c3429c52265832beab9df72a0ceae testout_tile_420_Q95_128x128.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} f0b5617d578f5e13c8eee215d64d4877 testout_tile_422_Q95_128x128.ppm && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_128x128.ppm

      cd /data/local/tmp && rm -rf cjpeg_djpeg_444_islow_prog_crop && mkdir cjpeg_djpeg_444_islow_prog_crop && \
        cp /data/local/tmp/libjpeg-turbo-images/testorig.ppm . && \
        /data/local/tmp/system/bin/cj${BUILD_SUFFIX} -dct int -prog -sample 1x1 -outfile testout_444_islow_prog.jpg testorig.ppm && \
        /data/local/tmp/system/bin/dj${BUILD_SUFFIX} -dct int -crop 98x98+13+13 -ppm -outfile testout_444_islow_prog_crop98x98,13,13.ppm testout_444_islow_prog.jpg && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} db87dc7ce26bcdc7a6b56239ce2b9d6c testout_444_islow_prog_crop98x98,13,13.ppm

      cd /data/local/tmp && rm -rf jpegtran_crop && mkdir jpegtran_crop && cd jpegtran_crop && \
        cp /data/local/tmp/libjpeg-turbo-images/testorig.jpg . && \
        /data/local/tmp/system/bin/jpegtran${BUILD_SUFFIX} -crop 120x90+20+50 -transpose -perfect -outfile testout_crop.jpg testorig.jpg && \
        /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} b4197f377e621c4e9b1d20471432610d testout_crop.jpg

      cd /data/local/tmp && rm -rf tjunittest && mkdir tjunittest && cd tjunittest && \
        /data/local/tmp/system/bin/tjunit${BUILD_SUFFIX} -alloc -nobuf
    done



libpdfium ("pdfium_test")
-------------------------

Build:

.. code-block:: sh

    m pdfium_test

    adb shell mkdir -p /data/local/tmp/system/bin
    adb shell mkdir -p /data/local/tmp/pdfium-testfiles

    for BUILD_SUFFIX in "" "-c64"; do
      adb push out/target/product/morello/system/bin/pdfium_test${BUILD_SUFFIX} /data/local/tmp/system/bin
    done

    for BUILD_SUFFIX in "64" "c64"; do
      adb shell mkdir -p /data/local/tmp/system/lib${BUILD_SUFFIX}

      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libft2.so /data/local/tmp/system/lib${BUILD_SUFFIX}
      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libjpeg.so /data/local/tmp/system/lib${BUILD_SUFFIX}
      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libpng.so /data/local/tmp/system/lib${BUILD_SUFFIX}
      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libz.so /data/local/tmp/system/lib${BUILD_SUFFIX}
    done

    adb push external/pdfium/testing/resources/*.pdf \
      /data/local/tmp/pdfium-testfiles

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    cd /data/local/tmp/pdfium-testfiles

    BIN_SUFFIX[0]=""
    BIN_SUFFIX[1]="-c64"
    LIB_SUFFIX[0]="64"
    LIB_SUFFIX[1]="c64"

    for BUILD_SUFFIX_INDEX in 0 1; do
      BUILD_SUFFIX_BIN=${BIN_SUFFIX[${BUILD_SUFFIX_INDEX}]}
      BUILD_SUFFIX_LIB=${LIB_SUFFIX[${BUILD_SUFFIX_INDEX}]}

      LD_LIBRARY_PATH=/data/local/tmp/system/lib${BUILD_SUFFIX_LIB} \
        /data/local/tmp/system/bin/pdfium_test${BUILD_SUFFIX_BIN} page_labels.pdf
    done


libpng ("pngtest")
------------------

Build:

.. code-block:: sh

    m pngtest

    adb shell mkdir -p /data/local/tmp/pngtest-files

    for BUILD_SUFFIX in 64 c64; do
      adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/pngtest/pngtest \
        /data/nativetest${BUILD_SUFFIX}/pngtest/pngtest

      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libpng.so \
        /data/nativetest${BUILD_SUFFIX}/pngtest/
      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libz.so \
        /data/nativetest${BUILD_SUFFIX}/pngtest/
    done

    adb push external/libpng/contrib/testpngs/*.png \
      /data/local/tmp/pngtest-files

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

  cd /data/local/tmp/pngtest-files

  for BUILD_SUFFIX in 64 c64; do
    LD_LIBRARY_PATH=/data/nativetest${BUILD_SUFFIX}/pngtest \
      /data/nativetest${BUILD_SUFFIX}/pngtest/pngtest rgb-alpha-16-sRGB.png
  done



zlib ("zlib_bench" and "minigzip")
----------------------------------

Build:

.. code-block:: sh

    m zlib_bench minigzip_target

    adb shell mkdir -p /data/local/tmp/system/bin

    for BUILD_SUFFIX in "" "-c64"; do
      adb push out/target/product/morello/system/bin/zlib_bench${BUILD_SUFFIX} /data/local/tmp/system/bin
      adb push out/target/product/morello/system/bin/minigzip${BUILD_SUFFIX} /data/local/tmp/system/bin
    done

    for BUILD_SUFFIX in "64" "c64"; do
      adb shell mkdir -p /data/local/tmp/system/lib${BUILD_SUFFIX}

      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libz.so /data/local/tmp/system/lib${BUILD_SUFFIX}
    done


Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    cd /data/local/tmp

    BIN_SUFFIX[0]=""
    BIN_SUFFIX[1]="-c64"
    LIB_SUFFIX[0]="64"
    LIB_SUFFIX[1]="c64"

    for BUILD_SUFFIX_INDEX in 0 1; do
      BUILD_SUFFIX_BIN=${BIN_SUFFIX[${BUILD_SUFFIX_INDEX}]}
      BUILD_SUFFIX_LIB=${LIB_SUFFIX[${BUILD_SUFFIX_INDEX}]}

      LD_LIBRARY_PATH=/data/local/tmp/system/lib${BUILD_SUFFIX_LIB} \
        /data/local/tmp/system/bin/zlib_bench${BUILD_SUFFIX_BIN} zlib --compression 1 \
        /data/local/tmp/system/bin/zlib_bench${BUILD_SUFFIX_BIN}

      LD_LIBRARY_PATH=/data/local/tmp/system/lib${BUILD_SUFFIX_LIB} \
        /data/local/tmp/system/bin/minigzip${BUILD_SUFFIX_BIN} \
          < /data/local/tmp/system/bin/minigzip${BUILD_SUFFIX_BIN} \
          > temp.gz

      LD_LIBRARY_PATH=/data/local/tmp/system/lib${BUILD_SUFFIX_LIB} \
        /data/local/tmp/system/bin/minigzip${BUILD_SUFFIX_BIN} -d \
          < temp.gz \
          > temp

      diff temp /data/local/tmp/system/bin/minigzip${BUILD_SUFFIX_BIN}
    done


BoringSSL ("boringssl_ssl_test" and "boringssl_crypto_test")
------------------------------------------------------------

Build:

.. code-block:: sh

    m boringssl_ssl_test boringssl_crypto_test

    for BUILD_SUFFIX in 64 c64; do
      adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test/boringssl_ssl_test \
        /data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test/boringssl_ssl_test
      adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/boringssl_crypto_test/boringssl_crypto_test \
        /data/nativetest${BUILD_SUFFIX}/boringssl_crypto_test/boringssl_crypto_test

      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libssl.so \
        /data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test/
      adb push out/target/product/morello/system/lib${BUILD_SUFFIX}/libcrypto.so \
        /data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test/
    done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    for BUILD_SUFFIX in 64 c64; do
      LD_LIBRARY_PATH=/data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test \
        /data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test/boringssl_ssl_test
      LD_LIBRARY_PATH=/data/nativetest${BUILD_SUFFIX}/boringssl_ssl_test \
        /data/nativetest${BUILD_SUFFIX}/boringssl_crypto_test/boringssl_crypto_test
    done


PCRE2 ("pcre2test" and "pcre2grep")
-----------------------------------

Build:

.. code-block: sh

    m pcre2test

    for BUILD_SUFFIX in 64 c64; do
      adb push out/target/product/morello/data/nativetest${BUILD_SUFFIX}/pcre2test \
        /data/nativetest${BUILD_SUFFIX}/pcre2test
      adb shell chmod +x \
        /data/nativetest${BUILD_SUFFIX}/pcre2test/pcre2test \
        /data/nativetest${BUILD_SUFFIX}/pcre2test/pcre2grep \
        /data/nativetest${BUILD_SUFFIX}/pcre2test/dist2/RunTest \
        /data/nativetest${BUILD_SUFFIX}/pcre2test/dist2/RunGrepTest
    done

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    for BUILD_SUFFIX in 64 c64; do
      cd /data/nativetest${BUILD_SUFFIX}/pcre2test
      srcdir=dist2 ./dist2/RunTest
      srcdir=dist2 ./dist2/RunGrepTest
    done

Basic demo of capability-based compartmentalization
---------------------------------------------------

Steps to build and run test

.. code-block:: sh

 m compartment-demo
 adb push out/target/product/morello/data/nativetest64/compartment-demo/ \
   /data/nativetest64/compartment-demo/

on FVP run the workloads using the commands lines below

.. code-block:: sh

 /data/nativetest64/compartment-demo/compartment-demo

.. _user guide:
 user-guide.rst
